// Hall sensor libraries
#include "TMAG5273.h"
#include <Arduino.h>
#include <Wire.h>

// uros libraries
#include <micro_ros_arduino.h>

#include <stdio.h>
#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>

#include <std_msgs/msg/int32_multi_array.h>

#include <rmw_microros/rmw_microros.h>


rcl_publisher_t publisher;
std_msgs__msg__Int32MultiArray msg;
rclc_executor_t executor;
rclc_support_t support;
rcl_allocator_t allocator;
rcl_node_t node;
rcl_timer_t timer;


#define LED_PIN 13

#define RCCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){error_loop();}}
#define RCSOFTCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){}}


// Hall setup
#define TMAG_SDA A4
#define TMAG_SCL A5

TMAG5273 tmag5273(&Wire);

// pins
int enable_pins[] = {2, 4, 7, 9, 10};//, 7, 9, 10};
unsigned int hex[] = {0x36, 0x37, 0x38, 0x39, 0x3A};//, 0x38, 0x35 + 4, 0x35 + 5};
const int num_sensors = 5;

// Sensor Data
float Bx[num_sensors], By[num_sensors], Bz[num_sensors], T[num_sensors];
float BxF, ByF, BzF, TF;
float Bx_[num_sensors], By_[num_sensors], Bz_[num_sensors], T_[num_sensors];

uint8_t res;

//LPF
double LPF_;

void error_loop(){
  while(1){
    digitalWrite(LED_PIN, !digitalRead(LED_PIN));
    delay(100);
  }
}

void timer_callback(rcl_timer_t * timer, int64_t last_call_time)
{  
  RCLC_UNUSED(last_call_time);
  if (timer != NULL) {
    RCSOFTCHECK(rcl_publish(&publisher, &msg, NULL));

    // READ HALL SENSORS
    for(int i = 0; i<num_sensors; i++) {
      tmag5273.switchSensor(hex[i]);
      res = tmag5273.readMagneticField(&Bx[i], &By[i], &Bz[i], &T[i]);
      BxF = LPF_/(LPF_+1)*Bx[i] + 1/(LPF_+1)*Bx_[i];
      ByF = LPF_/(LPF_+1)*By[i] + 1/(LPF_+1)*By_[i];
      BzF = LPF_/(LPF_+1)*Bz[i] + 1/(LPF_+1)*Bz_[i];
      TF = LPF_/(LPF_+1)*T[i] + 1/(LPF_+1)*T_[i];

      msg.data.data[3*i] = BxF * 100;
      msg.data.data[3*i + 1] = ByF * 100;
      msg.data.data[3*i + 2] = BzF * 100;
  
      Bx_[i]=BxF; By_[i]=ByF; Bz_[i]=BzF; T_[i]=TF;
    }
  }
}

void setup() {
  analogReadResolution(12);
  
  set_microros_transports();
  
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);  
  
  delay(2000);

  allocator = rcl_get_default_allocator();

  // Initialize and modify options (Set DOMAIN ID to 5)
  rcl_init_options_t init_options = rcl_get_zero_initialized_init_options();
  RCCHECK(rcl_init_options_init(&init_options, allocator));
  RCCHECK(rcl_init_options_set_domain_id(&init_options, 5));
  
  // Initialize rclc support object with custom options
  RCCHECK(rclc_support_init_with_options(&support, 0, NULL, &init_options, &allocator));
  RCCHECK(rclc_node_init_default(&node, "int32_publisher_rclc", "", &support));

  // create publisher
  RCCHECK(rclc_publisher_init_default(
    &publisher,
    &node,
    ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Int32MultiArray),
    "raw_hall_data"));

  // create timer,
  const unsigned int timer_timeout = 1;
  RCCHECK(rclc_timer_init_default(
    &timer,
    &support,
    RCL_MS_TO_NS(timer_timeout),
    timer_callback));

  // create executor
  RCCHECK(rclc_executor_init(&executor, &support.context, 1, &allocator));
  RCCHECK(rclc_executor_add_timer(&executor, &timer));

  // Assigning dynamic memory to the sequence
  msg.data.capacity = 100;
  msg.data.data = (int32_t*) malloc(msg.data.capacity * sizeof(int32_t));
  msg.data.size = 15;

  // Filling some data
  for(int32_t i = 0; i < 15; i++){
    msg.data.data[i] = 0;
  }
  
  ///////////////////////////////////////////////////////////////////
  // SETUP HALL SENSORS
  ///////////////////////////////////////////////////////////////////

  for(int i = 0; i<num_sensors; i++) {
    pinMode(enable_pins[i], OUTPUT);
    digitalWrite(enable_pins[i], LOW);
    Bx_[i] = 0; By_[i] = 0; Bz_[i] = 0;
  }

  
  Wire.begin();
  
  uint8_t arrayDevices = 0;
  uint8_t error;
  for(int i = 0; i<num_sensors; i++) {  
      digitalWrite(enable_pins[i], HIGH);
      delay(2);
      Wire.beginTransmission(TMAG5273_DEFAULT_ADDR);
      error = Wire.endTransmission();
      if (error == 0) {
          tmag5273.switchSensor(TMAG5273_DEFAULT_ADDR);
          tmag5273.modifyI2CAddress(TMAG5273_ARRAY_START_ADDR + arrayDevices);

          Wire.beginTransmission(TMAG5273_ARRAY_START_ADDR + arrayDevices);
          error = Wire.endTransmission();
          if (error == 0) {
              arrayDevices++;
              Serial.print(TMAG5273_ARRAY_START_ADDR + arrayDevices -1, HEX); Serial.print("  ok");
          }
          else{
            Serial.print(TMAG5273_ARRAY_START_ADDR + arrayDevices, HEX); Serial.print("  NOT ok!!");
          }
      }
      else{
            Serial.print(TMAG5273_ARRAY_START_ADDR + arrayDevices, HEX); Serial.print("  NOT ok!!");
      }

      Serial.println("");
  }

  
  tmag5273.configOperatingMode(TMAG5273_OPERATING_MODE_MEASURE);
  tmag5273.configReadMode(TMAG5273_READ_MODE_STANDARD);
  tmag5273.configMagRange(TMAG5273_MAG_RANGE_80MT);
  tmag5273.configLplnMode(TMAG5273_LOW_NOISE);
  tmag5273.configMagTempcoMode(TMAG5273_MAG_TEMPCO_NdBFe);
  tmag5273.configConvAvgMode(TMAG5273_CONV_AVG_1X);
  tmag5273.configTempChEnabled(true);
  tmag5273.initAll();

  double cutFreq = 50;
  double senFreq = 286;
  LPF_ = 2*PI*cutFreq/senFreq;
  
}

void loop() {
//  delay(100);
  RCSOFTCHECK(rclc_executor_spin_some(&executor, RCL_MS_TO_NS(100)));
}
