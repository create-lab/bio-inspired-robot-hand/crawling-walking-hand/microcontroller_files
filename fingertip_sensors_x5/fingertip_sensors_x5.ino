#include "TMAG5273.h"
#include <Arduino.h>
#include <Wire.h>

#define TMAG_SDA A4
#define TMAG_SCL A5

TMAG5273 tmag5273(&Wire);

// pins
int enable_pins[] = {2, 4, 7, 9, 10};//, 7, 9, 10};
unsigned int hex[] = {0x36, 0x37, 0x38, 0x39, 0x3A};//, 0x38, 0x35 + 4, 0x35 + 5};
const int num_sensors = 5;

// Sensor Data
float Bx[num_sensors], By[num_sensors], Bz[num_sensors], T[num_sensors];
float BxF, ByF, BzF, TF;
float Bx_[num_sensors], By_[num_sensors], Bz_[num_sensors], T_[num_sensors];

uint8_t res;

//LPF
double LPF_;

void setup() {
  
  Serial.begin(115200);
  Serial.println("aadsf");
  
  for(int i = 0; i<num_sensors; i++) {
    pinMode(enable_pins[i], OUTPUT);
    digitalWrite(enable_pins[i], LOW);
    Bx_[i] = 0; By_[i] = 0; Bz_[i] = 0;
  }

  
  Wire.begin();
  
  uint8_t arrayDevices = 0;
  uint8_t error;
  for(int i = 0; i<num_sensors; i++) {  
      digitalWrite(enable_pins[i], HIGH);
      delay(2);
      Wire.beginTransmission(TMAG5273_DEFAULT_ADDR);
      error = Wire.endTransmission();
      if (error == 0) {
          tmag5273.switchSensor(TMAG5273_DEFAULT_ADDR);
          tmag5273.modifyI2CAddress(TMAG5273_ARRAY_START_ADDR + arrayDevices);

          Wire.beginTransmission(TMAG5273_ARRAY_START_ADDR + arrayDevices);
          error = Wire.endTransmission();
          if (error == 0) {
              arrayDevices++;
              Serial.print(TMAG5273_ARRAY_START_ADDR + arrayDevices -1, HEX); Serial.print("  ok");
          }
          else{
            Serial.print(TMAG5273_ARRAY_START_ADDR + arrayDevices, HEX); Serial.print("  NOT ok!!");
          }
      }
      else{
            Serial.print(TMAG5273_ARRAY_START_ADDR + arrayDevices, HEX); Serial.print("  NOT ok!!");
      }

      Serial.println("");
  }

  
  tmag5273.configOperatingMode(TMAG5273_OPERATING_MODE_MEASURE);
  tmag5273.configReadMode(TMAG5273_READ_MODE_STANDARD);
  tmag5273.configMagRange(TMAG5273_MAG_RANGE_80MT);
  tmag5273.configLplnMode(TMAG5273_LOW_NOISE);
  tmag5273.configMagTempcoMode(TMAG5273_MAG_TEMPCO_NdBFe);
  tmag5273.configConvAvgMode(TMAG5273_CONV_AVG_1X);
  tmag5273.configTempChEnabled(true);
  tmag5273.initAll();

  double cutFreq = 50;
  double senFreq = 286;
  LPF_ = 2*PI*cutFreq/senFreq;

}


void loop() {
  Serial.println("loop");
  
  // Read Hall effect sensor data
  for(int i = 0; i<num_sensors; i++) {
    tmag5273.switchSensor(hex[i]);
    res = tmag5273.readMagneticField(&Bx[i], &By[i], &Bz[i], &T[i]);
    BxF = LPF_/(LPF_+1)*Bx[i] + 1/(LPF_+1)*Bx_[i];
    ByF = LPF_/(LPF_+1)*By[i] + 1/(LPF_+1)*By_[i];
    BzF = LPF_/(LPF_+1)*Bz[i] + 1/(LPF_+1)*Bz_[i];
    TF = LPF_/(LPF_+1)*T[i] + 1/(LPF_+1)*T_[i];

//    dataCarrier[3*i] = String(BxF);
//    dataCarrier[3*i + 1] = String(ByF);
//    dataCarrier[3*i + 2] = String(BzF);

    Serial.print(i); Serial.print(": "); Serial.print(BxF); Serial.print(" . ");
    Bx_[i]=BxF; By_[i]=ByF; Bz_[i]=BzF; T_[i]=TF;
  }

  Serial.println("");

  delay(500);
 
//  load_msg_to_python(msgName, dataCarrier, size_of_array(msgName));
//  sync();
}
